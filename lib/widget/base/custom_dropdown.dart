import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class CustomDropDown extends StatelessWidget {
  const CustomDropDown({
    super.key,
    required this.items,
    required this.firstItem,
    required this.title,
    required this.controller,
  });

  final List<String> items;
  final String firstItem;
  final String title;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(title).p12m().primary(),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        DropdownButtonFormField2(
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              //<-- SEE HERE
              borderRadius: const BorderRadius.all(
                Radius.circular(10),
              ),
              borderSide: BorderSide(
                color: OPrimaryColor,
              ),
            ),
            isDense: true,
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(
                30,
              ),
            ),
          ),
          isExpanded: true,
          hint: Text(
            firstItem,
            style: GoogleFonts.poppins(
              color: const Color(0xff868686),
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
          ),
          items: items
              .map(
                (item) => DropdownMenuItem<String>(
                  value: item,
                  child: Text(
                    item,
                    style: GoogleFonts.poppins(
                      color: const Color(0xff868686),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              )
              .toList(),
          validator: (value) {
            if (value == null) {
              return 'Please select item.';
            }
            return null;
          },
          onChanged: (value) {
            // selectedValue = value.toString();
            controller.text = value.toString();
            // print(widget.controller.text);

            // widget.controller.text = selectedValue!;
            //Do something when changing the item if you want.
          },
          onSaved: (value) {
            // selectedValue = value.toString();
            // widget.controller.value = selectedValue as TextEditingValue;
            // widget.controller.text = selectedValue!;
            // print(widget.controller.text);
          },
          // itemPadding: EdgeInsets.only(left: 20, right: 10),
          // itemHeight: 60,
          // buttonPadding: EdgeInsets.only(left: 20, right: 10),
          buttonStyleData: const ButtonStyleData(
            height: 38,
            padding: EdgeInsets.only(
              right: 10,
            ),
          ),
          // dropdownDecoration: BoxDecoration(
          //   borderRadius: BorderRadius.circular(15),
          // ),
          iconStyleData: IconStyleData(
            icon: Icon(
              Icons.arrow_drop_down,
              color: OPrimaryColor,
            ),
            iconSize: 30,
          ),
          dropdownStyleData: DropdownStyleData(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      ],
    );
  }
}
