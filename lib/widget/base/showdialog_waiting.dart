import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tagbeta/utils/colors.dart';

waitingDialog({
  required bool barier,
  required BuildContext context,
  required String title,
  FontWeight? textWeight,
  Color? textColor,
  double? textSize,
  IconData? icon,
}) {
  return showDialog<void>(
    context: context,
    barrierDismissible: barier,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
        actions: <Widget>[
          Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Column(
                children: [
                  Container(
                    width: Get.width,
                    height: 40,
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: (textSize == null) ? 14 : textSize,
                        color: (textColor == null) ? OPrimaryColor : textColor,
                        fontWeight: (textWeight == null) ? FontWeight.w700 : textWeight,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Icon((icon == null) ? Icons.timelapse_outlined : icon)
                ],
              ),
              // const SizedBox(
              //   height: 20,
              // ),
            ],
          )
        ],
      );
    },
  );
}
