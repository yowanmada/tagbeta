import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tagbeta/utils/colors.dart';

import '../../controllers/global_controller.dart';

class CustomForm extends StatefulWidget {
  const CustomForm({super.key, required this.controller, required this.hintText, required this.title, this.fillColor, this.isMust, this.editable});
  final TextEditingController controller;
  final String hintText;
  final String title;
  final bool? editable;
  final bool? fillColor;
  final bool? isMust;

  @override
  State<CustomForm> createState() => _CustomFormState();
}

class _CustomFormState extends State<CustomForm> {
  final c = Get.put(GlobalController());
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.white,
          width: Get.width,
          height: 20,
          child: RichText(
            text: TextSpan(
              text: widget.title,
              style: GoogleFonts.poppins(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w600),
              children: <TextSpan>[
                TextSpan(
                  text: (widget.isMust == true) ? '*' : '',
                  style: GoogleFonts.poppins(color: const Color(0xffF1416C), fontSize: 12, fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Container(
          height: 37,
          alignment: Alignment.center,
          child: TextField(
            enabled: (widget.editable == null) ? true : widget.editable,
            controller: widget.controller,
            textAlignVertical: TextAlignVertical.center,
            style: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
            decoration: InputDecoration(
              hintStyle: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
              hintText: widget.hintText,
              filled: true,
              fillColor: (widget.fillColor == null) ? Colors.white : const Color(0xffD0F4FF),
              contentPadding: const EdgeInsets.only(bottom: 0, left: 15),
              focusedBorder: OutlineInputBorder(
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(width: 1, color: OPrimaryColor),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 1, color: OPrimaryColor), //<-- SEE HERE
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        )
      ],
    );
  }
}
