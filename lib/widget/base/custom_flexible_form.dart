import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tagbeta/utils/colors.dart';

class FlexibleTexField extends StatefulWidget {
  const FlexibleTexField({super.key, required this.controller, required this.hintText, required this.title, this.isMust});
  final TextEditingController controller;
  final String hintText;
  final String title;
  final bool? isMust;

  @override
  State<FlexibleTexField> createState() => _FlexibleTexFieldState();
}

class _FlexibleTexFieldState extends State<FlexibleTexField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.white,
          width: Get.width,
          height: 20,
          child: RichText(
            text: TextSpan(
              text: widget.title,
              style: GoogleFonts.poppins(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w600),
              children: <TextSpan>[
                TextSpan(
                  text: (widget.isMust == true) ? '*' : '',
                  style: GoogleFonts.poppins(color: const Color(0xffF1416C), fontSize: 12, fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 2,
        ),
        Container(
          // margin: EdgeInsets.symmetric(vertical: 10),
          // height: 37,
          alignment: Alignment.center,
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxHeight: 300.0,
              minHeight: 15,
            ),
            child: TextField(
              enabled: true,
              maxLines: null,
              controller: widget.controller,
              textAlignVertical: TextAlignVertical.center,
              style: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
              decoration: InputDecoration(
                hintStyle: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                // hintText: widget.hintText,
                filled: true,
                fillColor: Colors.transparent,
                contentPadding: const EdgeInsets.only(bottom: 0, left: 15),
                focusedBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(width: 1, color: OPrimaryColor),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: OPrimaryColor), //<-- SEE HERE
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
      ],
    );
  }
}
