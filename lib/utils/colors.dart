// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

Color OColorBrown = const Color(0xffB4A9A7);
Color OConcenGrey = const Color(0xffB4A9A7);
Color OCalmGrey = const Color(0xffE8E8E8);

Color OPrimaryColor = const Color(0xff002D3D);
Color OSecondaryColor = const Color(0xff3FAE7D);
Color OThirdColor = const Color(0xff51DB51);
Color OSnackColor = const Color(0xff51DB51);

Color OSuccess = const Color(0xff289D3D);

Color ONetralBlack = const Color(0xff050B16);
Color OCalmBlack = const Color(0xff0F1721);
Color ONetralwhite = const Color(0xffFFFFFF);
Color ORedColor = const Color(0xffFF4342);

Color OHeadingColor = const Color(0xff1D1919);
Color OTextColor = const Color(0xff002D3D);
Color OTextSecondColor = const Color(0xffFFA903);
