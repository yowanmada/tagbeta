// To parse this JSON data, do
//
//     final responQueue = responQueueFromJson(jsonString);

import 'dart:convert';

News responQueueFromJson(String str) => News.fromJson(json.decode(str));

String responQueueToJson(News data) => json.encode(data.toJson());

class News {
  News({
    required this.path,
    required this.description,
  });

  String path;
  String description;

  factory News.fromJson(Map<String, dynamic> json) => News(
        path: json["patient_id"] ?? "",
        description: json["doctor_id"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "patient_id": path,
        "doctor_id": description,
      };
  static List<News> listFromJson(List<dynamic> list) => List<News>.from(list.map((x) => News.fromJson(x)));
}
