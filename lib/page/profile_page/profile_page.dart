import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/page/profile_page/profile_edit_page/profile_edit_page.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/showdialog_two_button.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Profile Page").p14b().white(),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Container(
              width: 25,
              height: 25,
              decoration: BoxDecoration(
                color: OSecondaryColor,
                borderRadius: const BorderRadius.all(
                  Radius.circular(1000),
                ),
              ),
              child: Icon(
                Icons.done,
                color: OPrimaryColor,
              ),
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            height: 270,
            width: Get.width,
            decoration: BoxDecoration(
              color: OPrimaryColor,
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(1000),
                bottomRight: Radius.circular(1000),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: Get.width * 0.7,
                    decoration: BoxDecoration(
                      color: ONetralwhite,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: OConcenGrey,
                          offset: const Offset(
                            3.0,
                            3.0,
                          ),
                          blurRadius: 1.0,
                          spreadRadius: 1.0,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 10.0,
                        horizontal: 8,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Get.to(const ProfileEditPage());
                                },
                                child: Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                    color: OPrimaryColor,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(1000),
                                    ),
                                  ),
                                  child: Icon(
                                    Icons.edit,
                                    size: 18,
                                    color: ONetralwhite,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(1000),
                            ),
                            child: SizedBox(
                              width: 158,
                              height: 158,
                              child: FittedBox(
                                fit: BoxFit.cover,
                                child: Image.asset(
                                  "assets/images/gambar1.jpg",
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                            overflow: TextOverflow.ellipsis,
                            "Betran Putera Sinaga",
                            style: TextStyle(height: 1.5),
                            maxLines: 1,
                          ).p14b().primary(),
                          const SizedBox(
                            height: 2,
                          ),
                          const Text(
                            overflow: TextOverflow.ellipsis,
                            "0985656165165",
                            style: TextStyle(height: 1.5),
                            maxLines: 1,
                          ).p12r().primary(),
                          const SizedBox(
                            height: 2,
                          ),
                          const Text(
                            overflow: TextOverflow.ellipsis,
                            "byayoan@gmail.com",
                            style: TextStyle(height: 1.5),
                            maxLines: 1,
                          ).p12r().primary(),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                            overflow: TextOverflow.ellipsis,
                            "Jalan Klampis Harapan VI Klampis ngasem, Sukolilo Kota Surabaya 601189, Jawa TImur",
                            style: TextStyle(height: 1.5),
                            maxLines: 3,
                            textAlign: TextAlign.center,
                          ).p12r().primary(),
                          const SizedBox(
                            height: 5,
                          ),
                          BaseButton(
                            ontap: () {
                              Get.back();
                              dialogBuilder(
                                context: context,
                                buttonLeft: () {
                                  Get.back();
                                  Get.snackbar(
                                    "Log Out Success",
                                    "Silahkan Login Kembali :)",
                                    colorText: Colors.white,
                                    backgroundColor: ORedColor,
                                    icon: const Icon(Icons.logout),
                                  );
                                },
                                buttonRight: () {
                                  Get.back();
                                },
                                title: "Anda Yakin Ingin Logout ?",
                                leftTittle: 'Ya',
                                rightTitle: "Tidak",
                                barier: true,
                              );
                            },
                            text: "Keluar",
                            height: 22,
                            textSize: 10,
                            outlineRadius: 20,
                            textWeight: FontWeight.w700,
                            color: ORedColor,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: Get.width * 0.7,
                    decoration: BoxDecoration(
                      color: ONetralwhite,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: OConcenGrey,
                          offset: const Offset(
                            3.0,
                            3.0,
                          ),
                          blurRadius: 1.0,
                          spreadRadius: 1.0,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 10.0,
                        horizontal: 8,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            overflow: TextOverflow.ellipsis,
                            "Versi 1.0.0",
                            style: TextStyle(height: 1.5),
                            maxLines: 1,
                          ).p12r().primary(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
