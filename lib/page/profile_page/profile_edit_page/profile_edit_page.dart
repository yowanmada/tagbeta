import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/custom_dropdown.dart';
import 'package:tagbeta/widget/base/custom_form.dart';
import 'package:tagbeta/widget/base/showdialog_waiting.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class ProfileEditPage extends StatefulWidget {
  const ProfileEditPage({super.key});

  @override
  State<ProfileEditPage> createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  File? filePhoto;
  final ImagePicker _picker = ImagePicker();
  TextEditingController namaController = TextEditingController(text: "");
  TextEditingController kotaController = TextEditingController(text: "");
  TextEditingController provinsiController = TextEditingController(text: "");
  TextEditingController kecamatanController = TextEditingController(text: "");
  TextEditingController kelurahanController = TextEditingController(text: "");
  var items = ["jawa timur", "Jawa barat", "jawa tengah", "jawa selatan"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Profile Edit Page").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: Stack(
        children: [
          Container(
            height: 270,
            width: Get.width,
            decoration: BoxDecoration(
              color: OPrimaryColor,
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(1000),
                bottomRight: Radius.circular(1000),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: Get.width * 0.85,
                      decoration: BoxDecoration(
                        color: ONetralwhite,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: OConcenGrey,
                            offset: const Offset(
                              3.0,
                              3.0,
                            ),
                            blurRadius: 1.0,
                            spreadRadius: 1.0,
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 10.0,
                          horizontal: 8,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(children: [
                              Column(
                                children: [
                                  ClipRRect(
                                    borderRadius: const BorderRadius.all(Radius.circular(1000)),
                                    child: SizedBox(
                                      width: 136,
                                      height: 136,
                                      child: FittedBox(
                                        fit: BoxFit.cover,
                                        child: (filePhoto == null)
                                            ? FittedBox(
                                                fit: BoxFit.fitWidth,
                                                child: Container(
                                                  color: Colors.grey,
                                                  child: const Icon(
                                                    Icons.people,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              )
                                            // FadeInImage(
                                            //     image: NetworkImage(widget.patient.picture),
                                            //     placeholder: const AssetImage("assets/images/img_avatar.png"),
                                            //     imageErrorBuilder: (context, error, stackTrace) {
                                            //       return Image.asset('assets/images/img_avatar.png', fit: BoxFit.fitWidth);
                                            //     },
                                            //     fit: BoxFit.cover,
                                            //   )
                                            : Image.file(
                                                File(filePhoto!.path),
                                                fit: BoxFit.fitWidth,
                                              ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Positioned(
                                bottom: 2,
                                right: 0,
                                child: GestureDetector(
                                  onTap: () async {
                                    showDialog(
                                      barrierDismissible: true,
                                      context: context,
                                      builder: (c) => AlertDialog(
                                        // title: Container(),
                                        content: Container(
                                          // color: Color.fromRGBO(255, 255, 255, 1),
                                          color: ONetralwhite,
                                          height: 80,
                                          child: Column(
                                            // mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              InkWell(
                                                onTap: () async {
                                                  Navigator.pop(context);
                                                  // File? file = await Get.to(() => const CameraOverlay('identitas'));
                                                  final XFile? image = await _picker.pickImage(
                                                    source: ImageSource.camera,
                                                  );
                                                  if (image != null) {
                                                    setState(() {
                                                      filePhoto = File(image.path);
                                                      // p.filePhoto = File(image.path);

                                                      // filePhoto = image as File?;
                                                    });
                                                  }
                                                },
                                                child: const Padding(
                                                  padding: EdgeInsets.symmetric(vertical: 10.0),
                                                  child: Text(
                                                    "Kamera",
                                                  ),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () async {
                                                  Navigator.pop(context);
                                                  final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
                                                  if (image != null) {
                                                    setState(() {
                                                      filePhoto = File(image.path);
                                                      // p.filePhoto = File(image.path);
                                                      // print(p.filePhoto.toString());
                                                    });
                                                  }
                                                },
                                                child: const Text("Galeri"),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: 39,
                                    height: 39,
                                    decoration: BoxDecoration(
                                      color: OPrimaryColor,
                                      borderRadius: BorderRadius.circular(1000),
                                      boxShadow: const [
                                        // BoxShadow(
                                        //   color: Color.fromARGB(83, 46, 46, 46),
                                        //   spreadRadius: 1,
                                        //   blurRadius: 1,
                                        //   offset: Offset(0.0, 1.0),
                                        // ),
                                      ],
                                      border: Border.all(color: Colors.white, width: 2),
                                    ),
                                    child: const Icon(
                                      Icons.camera_alt_rounded,
                                      color: Colors.white,
                                      size: 17,
                                    ),
                                  ),
                                ),
                              )
                            ]),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomForm(
                              controller: namaController,
                              // hintText: "",
                              hintText: "Betran Putera Siregar",
                              title: "Nama Lengkap",
                              isMust: true,
                            ),
                            CustomForm(
                              controller: namaController,
                              // hintText: "",
                              hintText: "0865456516",
                              title: "Nomor Telepon",
                              isMust: true,
                            ),
                            BaseButton(
                              ontap: () {},
                              text: "Simpan",
                              height: 25,
                              textSize: 10,
                              outlineRadius: 20,
                              textWeight: FontWeight.w700,
                              color: OSecondaryColor,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            CustomDropDown(
                              title: "Provinsi",
                              items: items,
                              firstItem: items[0],
                              controller: provinsiController,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomDropDown(
                              title: "Kota",
                              items: items,
                              firstItem: items[0],
                              controller: kotaController,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomDropDown(
                              title: "Kecamatan",
                              items: items,
                              firstItem: items[0],
                              controller: kecamatanController,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomDropDown(
                              title: "Kelurahan",
                              items: items,
                              firstItem: items[0],
                              controller: kelurahanController,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomForm(
                              controller: namaController,
                              // hintText: "",
                              hintText: "Kota Klampis Ngasem",
                              title: "Jalan",
                              isMust: false,
                            ),
                            CustomForm(
                              controller: namaController,
                              // hintText: "",
                              hintText: "60199",
                              title: "Kodepos",
                              isMust: false,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            ClipRRect(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(10),
                              ),
                              child: SizedBox(
                                width: Get.width,
                                height: 133,
                                child: FittedBox(
                                  fit: BoxFit.cover,
                                  child: Image.asset(
                                    "assets/images/bigmap.png",
                                  ),
                                ),
                              ),
                            ),
                            // Container(
                            //   height: 133,
                            //   width: Get.width,
                            //   decoration: BoxDecoration(
                            //     borderRadius: BorderRadius.circular(20),
                            //   ),
                            // ),
                            const SizedBox(
                              height: 10,
                            ),
                            BaseButton(
                              ontap: () {},
                              text: "Tandai Lokasi Anda Saat Ini",
                              textColor: ONetralBlack,
                              height: 25,
                              textSize: 10,
                              outlineRadius: 20,
                              textWeight: FontWeight.w700,
                              color: OTextSecondColor,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            BaseButton(
                              ontap: () {
                                waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
                                Future.delayed(
                                  const Duration(seconds: 2),
                                  () async {
                                    Get.back();
                                    Navigator.pop(context);
                                  },
                                );
                              },
                              text: "Simpan",
                              height: 25,
                              textSize: 10,
                              outlineRadius: 20,
                              textWeight: FontWeight.w700,
                              color: OSecondaryColor,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            BaseButton(
                              ontap: () {
                                Get.back();
                              },
                              text: "Batal",
                              height: 25,
                              textSize: 10,
                              outlineRadius: 20,
                              textWeight: FontWeight.w700,
                              color: ORedColor,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
