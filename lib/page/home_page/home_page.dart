import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:tagbeta/page/news_page/news_detail_page/news_detail_page.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/showdialog_two_button.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controller = PageController(
    viewportFraction: 1,
    keepPage: true,
  );
  double screenWidth = Get.width; // screen width
  double screenPad = 16.0;
  List<String> reportType = [
    "COVID 19",
    "Bencana Alam",
    "Darurat Medis",
    "Kebakaran",
    "Kecelakaan",
    "KDRT",
    "Pembegalan",
    "Jambak-Jambak an",
    "Metamorfosis",
    "Aksi Penghancuran",
    "Fasilitas Publik Rusak",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("TAGBETA").p20b().white(),
        titleSpacing: 5,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: GestureDetector(
              onTap: () {
                customShowModalButtomSheet(context);
              },
              child: Icon(
                Icons.notifications_active,
                color: ONetralwhite,
              ),
            ),
          ),
        ],
        leading: Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Image.asset(
            "assets/images/icon.png",
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            HorizontalImageScroll(
              controller: controller,
              screenPad: screenPad,
            ),
            const SizedBox(
              height: 16,
            ),
            const NewsList()
          ],
        ),
      ),
    );
  }

  Future<void> customShowModalButtomSheet(BuildContext context) {
    return showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: Get.height * 0.6,
          width: Get.width,
          color: ONetralwhite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16),
                child: const Text('Laporkan Kejadian : ').p14r().primary(),
              ),
              Expanded(
                flex: 1,
                child: ListView.builder(
                  itemCount: reportType.length,
                  padding: EdgeInsets.zero,
                  itemBuilder: (BuildContext context, int index) => Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Get.back();
                          dialogBuilder(
                            context: context,
                            buttonLeft: () {
                              print("YAYAYAYAa");
                              Get.back();
                              Get.snackbar(
                                "Laporan Terkirim",
                                "Laporan Telah dikirim, tunggu Feedback dari Tim & Mohon Bersabar",
                                // snackStyle: ,
                                duration: Duration(seconds: 5),
                                colorText: Colors.white,
                                backgroundColor: OSnackColor,
                                icon: const Icon(Icons.notifications_active),
                              );
                            },
                            buttonRight: () {
                              print("TIDAK");
                              Get.back();
                            },
                            title: "Anda Sudah Memastikan Kejadian Benar Adanya dan Bukan Hoax ?",
                            leftTittle: 'Ya',
                            rightTitle: "Tidak",
                            barier: true,
                          );
                          // Get.to(NewsDetailPage());
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                overflow: TextOverflow.ellipsis,
                                reportType[index],
                                // style: TextStyle(height: 1.5),
                              ).p12m().primary(),
                              Icon(
                                Icons.notifications_active,
                                color: OPrimaryColor,
                                size: 15,
                              )
                            ],
                          ),
                        ),
                      ),
                      const Divider(
                        height: 2,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class NewsList extends StatelessWidget {
  const NewsList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    List<Artikel> articels = [
      Artikel(
        id: "01",
        title: "Kenali dan Pahami 10 Tanda Masalah Gigi & Mulut",
        image: "assets/images/img_artikel.png",
        desc: "Infrastruktur Kandangan raya bulak banteng lebih diperuntukan pada pengguna mobil pajero sprot dengan versi terbaru yakni TDR Magnum 20 hectar sawah Berbagai Hunian",
      ),
      Artikel(id: "02", title: "Koreksi Gangguan Penglihatan dengan Lensa Kontak", image: "assets/images/img_artikel2.png", desc: "Infrastruktur Kandangan raya bulak banteng lebih diperuntukan pada pengguna mobil pajero sprot dengan versi terbaru yakni TDR Magnum 20 hectar sawah Berbagai Hunian"),
      Artikel(id: "03", title: "Kenali Tanda-Tanda Mata Kamu Sedang Bermasalah", image: "assets/images/img_artikel3.png", desc: "Warga Kupang Lima Simo Indah Jaya Barat mendapat kecaman karena maraknya pembuangan sampah sembarangan oleh warga kampung bunian sangat menegangkan dan mengasyikkan hiu hiu"),
      Artikel(id: "04", title: "Kenali Tanda-Tanda Mata Kamu Sedang Bermasalah", image: "assets/images/img_artikel3.png", desc: "Warga Kupang Lima Simo Indah Jaya Barat mendapat kecaman karena maraknya pembuangan sampah sembarangan oleh warga kampung desa wisata pulau lombok yang indah nan cantik sekali."),
      Artikel(id: "05", title: "Kenali Tanda-Tanda Mata Kamu Sedang Bermasalah", image: "assets/images/img_artikel3.png", desc: "Warga Kupang Lima Simo Indah Jaya Barat mendapat kecaman karena maraknya pembuangan sampah sembarangan oleh warga kampung desa wisata pulau lombok yang indah nan cantik sekali."),
      Artikel(id: "06", title: "Kenali Tanda-Tanda Mata Kamu Sedang Bermasalah", image: "assets/images/img_artikel3.png", desc: "Infrastruktur Kandangan raya bulak banteng lebih diperuntukan pada pengguna mobil pajero sprot dengan versi terbaru yakni TDR Magnum 20 hectar sawah Berbagai Hunian"),
    ];
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "Berita Terbaru",
              ).p14b().primary(),
              GestureDetector(
                onTap: () {},
                child: Row(
                  children: [
                    const Text(
                      "Lihat Semua",
                    ).p10r().primary(),
                    const Icon(
                      Icons.keyboard_arrow_right_rounded,
                      size: 12,
                    )
                  ],
                ),
              )
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          ListView.builder(
            physics: const ScrollPhysics(),
            shrinkWrap: true,
            itemCount: articels.length,
            padding: EdgeInsets.zero,
            itemBuilder: (BuildContext context, int index) => Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: ClipRRect(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        child: SizedBox(
                          width: 110,
                          height: 110,
                          child: FittedBox(
                            fit: BoxFit.cover,
                            child: Image.asset(
                              "assets/images/gambar1.jpg",
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        height: 115,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  overflow: TextOverflow.ellipsis,
                                  articels[index].title,
                                  style: const TextStyle(height: 1.5),
                                  // maxLines: 1,
                                  // softWrap: true,
                                ).p12b().primary(),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  overflow: TextOverflow.ellipsis,
                                  articels[index].desc,
                                  style: const TextStyle(height: 1.5),
                                  maxLines: 4,
                                ).p10r().primary(),
                              ],
                            ),
                            const Spacer(),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Get.to(NewsDetailPage());
                                  },
                                  child: const Text(
                                    overflow: TextOverflow.ellipsis,
                                    "Baca Selengkapnya",
                                    style: TextStyle(height: 1.5),
                                  ).p9r().secondary(),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 3,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Artikel {
  final String id;
  final String title;
  final String desc;
  final String image;

  Artikel({
    required this.id,
    required this.title,
    required this.desc,
    required this.image,
  });

  @override
  String toString() {
    return '{ $title, $id, $desc }';
  }
}

class HorizontalImageScroll extends StatelessWidget {
  const HorizontalImageScroll({
    super.key,
    required this.controller,
    required this.screenPad,
  });

  final PageController controller;
  final double screenPad;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 8.0,
          ),
          child: SizedBox(
            height: 270,
            child: PageView.builder(
              controller: controller,
              itemCount: 4,
              allowImplicitScrolling: true,
              pageSnapping: true,
              // reverse: true,
              physics: const BouncingScrollPhysics(),
              itemBuilder: (
                BuildContext context,
                int index,
              ) {
                return Padding(
                  padding: EdgeInsets.only(
                    left: screenPad,
                    right: screenPad,
                  ),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                    child: Container(
                      width: Get.width,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      margin: const EdgeInsets.symmetric(
                        horizontal: 2,
                      ),
                      child: FittedBox(
                        fit: BoxFit.cover,
                        child: Image.asset(
                          "assets/images/gambar${index + 1}.jpg",
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          child: SmoothPageIndicator(
            controller: controller,
            count: 4,
            effect: ExpandingDotsEffect(
              activeDotColor: OPrimaryColor,
              dotHeight: 8,
              spacing: 6,
            ),
          ),
        ),
      ],
    );
  }
}
