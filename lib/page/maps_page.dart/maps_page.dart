import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tagbeta/controllers/map_controller.dart';
import 'package:tagbeta/page/news_page/news_detail_page/news_detail_page.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class MapsPage extends StatefulWidget {
  const MapsPage({super.key});

  @override
  State<MapsPage> createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  final m = Get.put(MapController());
  List<LatLng> latlngList = [
    LatLng(-7.248004, 112.631985),
    LatLng(-7.316795, 112.559200),
    LatLng(-7.288873, 112.796948),
    LatLng(-7.316795, 112.559200),
  ];

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> markers = Set();
  late CameraPosition _kLake;

  @override
  void initState() {
    SchedulerBinding.instance.scheduleFrameCallback((timeStamp) async {
      addMarkers();
    });
    setState(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text('Persebaran Lokasi Kasus').p14b().white(),
        elevation: 2,
      ),
      body: Stack(
        children: [
          Obx(
            () => GoogleMap(
              mapType: MapType.normal,
              myLocationEnabled: true,
              myLocationButtonEnabled: true,
              cameraTargetBounds: CameraTargetBounds.unbounded,
              compassEnabled: true,
              indoorViewEnabled: true,
              initialCameraPosition: m.initialCameraPosition.value,
              markers: markers, // Call a function to create markers
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BaseButton(
                    ontap: _goToTheLake,
                    text: "Refresh",
                    icon: Icons.refresh,
                    iconColor: ONetralwhite,
                    textSize: 14,
                    color: OTextSecondColor,
                    outlineRadius: 20,
                    textWeight: FontWeight.w400,
                    width: 140,
                    height: 40,
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              )
            ],
          ),
        ],
      ),
    );
  }

  Future<Position> getUserCurrentLocation() async {
    await Geolocator.requestPermission().then((value) {}).onError((error, stackTrace) async {
      await Geolocator.requestPermission();
      print("ERROR" + error.toString());
    });
    return await Geolocator.getCurrentPosition();
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.moveCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  addMarkers() async {
    BitmapDescriptor markerbitmap = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(5, 5)),
      "assets/images/issueLocation.png",
    );
    BitmapDescriptor markerbitmape = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(5, 5)),
      "assets/images/myLocation.png",
    );
    int count = 4;
    for (int i = 0; i < count; i++) {
      markers.add(
        Marker(
          markerId: MarkerId(i.toString()),
          position: latlngList[i], //position of marker
          infoWindow: InfoWindow(
            title: 'Point ${i + 1}',
          ),
          onTap: () {
            _showMarkerInfo(context);
          },
          icon: markerbitmap, //Icon for Marker
        ),
      );
    }
    getUserCurrentLocation().then((value) async {
      setState(() {
        markers.add(Marker(
          markerId: MarkerId(value.toString()),
          position: LatLng(value.latitude, value.longitude),
          infoWindow: InfoWindow(
            title: 'My Current Location',
          ),
          icon: markerbitmape,
        ));
        _kLake = CameraPosition(
          target: LatLng(value.latitude, value.longitude), // Ganti dengan koordinat yang Anda inginkan
          zoom: 9.0, // Ganti dengan level zoom yang Anda inginkan
        );
      });
    });

    setState(() {});
  }

  void _showMarkerInfo(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Kerusuhan Warga').p14b().primary(),
          actionsPadding: const EdgeInsets.symmetric(horizontal: 20),
          actions: [
            SizedBox(
              width: Get.width,
              height: 72,
              child: ListView.builder(
                itemCount: 4,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) => Row(
                  children: [
                    Row(
                      children: [
                        ClipRRect(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(10),
                          ),
                          child: SizedBox(
                            width: 60,
                            height: 60,
                            child: FittedBox(
                              fit: BoxFit.cover,
                              child: Image.asset(
                                "assets/images/gambar${index + 1}.jpg",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Text('Keterangan').p12m().primary(),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 55,
              width: Get.width,
              child: const Text(
                'Warga Kupang Lima Simo Indah Jaya Barat mendapat kecaman karena maraknya pembuangan sampah sembarangan!',
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.justify,
                maxLines: 3,
              ).p12r().primary(),
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: BaseButton(
                    ontap: () {
                      Get.to(const NewsDetailPage());
                    },
                    text: "Baca",
                    height: 26,
                    textSize: 10,
                    outlineRadius: 20,
                    textWeight: FontWeight.w700,
                    color: OSecondaryColor,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 1,
                  child: BaseButton(
                    ontap: () {
                      Get.back();
                    },
                    text: "Close",
                    height: 26,
                    textSize: 10,
                    outlineRadius: 20,
                    textWeight: FontWeight.w700,
                    color: OPrimaryColor,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }
}
