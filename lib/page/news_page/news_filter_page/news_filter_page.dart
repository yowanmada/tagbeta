import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/showdialog_waiting.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsFilterPage extends StatefulWidget {
  const NewsFilterPage({super.key});

  @override
  State<NewsFilterPage> createState() => _NewsFilterPageState();
}

class _NewsFilterPageState extends State<NewsFilterPage> {
  List<String> reportType = ["COVID 19", "Bencana Alam", "Darurat Medis", "Kebakaran", "Kecelakaan", "KDRT", "Pembegalan", "Jambak-Jambak an", "Metamorfosis", "Aksi Penghancuran", "Fasilitas Publik Rusak", "COVID 19", "Bencana Alam", "Darurat Medis", "Pembegalan", "Jambak-Jambak an"];
  List<bool> listBool = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
  var listTrue = [];
  // bool _isLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Filter & Urutan").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: Get.height,
                    width: Get.width,
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const Text("Kanal :").p14b().primary(),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            flex: 1,
                            child: ListView.builder(
                              itemCount: reportType.length,
                              padding: EdgeInsets.zero,
                              itemBuilder: (BuildContext context, int index) => Column(
                                children: [
                                  GestureDetector(
                                    onTap: () {},
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          overflow: TextOverflow.ellipsis,
                                          reportType[index],
                                          // style: TextStyle(height: 1.5),
                                        ).p12m().primary(),
                                        SizedBox(
                                          width: 45,
                                          height: 35,
                                          child: FittedBox(
                                            fit: BoxFit.fill,
                                            child: Switch(
                                              value: listBool[index],
                                              onChanged: (value) {
                                                setState(
                                                  () {
                                                    listBool[index] = !listBool[index];
                                                  },
                                                );
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  const Divider(
                                    height: 2,
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  if (index == reportType.length - 1) ...[
                                    const SizedBox(
                                      height: 180,
                                    ),
                                  ]
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  color: ONetralwhite,
                  height: 60,
                  width: Get.width,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: BaseButton(
                            ontap: () {
                              Future.delayed(const Duration(seconds: 2), () async {
                                setState(() {
                                  for (int i = 0; i < listBool.length; i++) {
                                    listBool[i] = false;
                                  }
                                });
                              });
                            },
                            text: "Reset",
                            outlineColor: ORedColor,
                            textColor: ORedColor,
                            color: ONetralwhite,
                            textSize: 14,
                            textWeight: FontWeight.w400,
                            outlineRadius: 20,
                            height: 35,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          flex: 1,
                          child: BaseButton(
                            ontap: () {
                              waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
                              Future.delayed(
                                const Duration(seconds: 2),
                                () async {
                                  setState(
                                    () {
                                      for (int i = 0; i < listBool.length; i++) {
                                        if (listBool[i] == true) {
                                          listTrue.add(i);
                                        }
                                      }
                                    },
                                  );
                                  Navigator.pop(
                                    context,
                                  );
                                  Navigator.pop(context, listTrue);
                                },
                              );
                            },
                            text: "Tampilkan",
                            textSize: 14,
                            textWeight: FontWeight.w400,
                            outlineRadius: 20,
                            height: 35,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
