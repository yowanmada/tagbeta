import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tagbeta/controllers/map_controller.dart';
import 'package:tagbeta/controllers/news_controller.dart';
import 'package:tagbeta/page/navigationbar/navigationbar.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/custom_flexible_form.dart';
import 'package:tagbeta/widget/base/showdialog_two_button.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsLocationPage extends StatefulWidget {
  const NewsLocationPage({super.key});
  @override
  State<NewsLocationPage> createState() => _NewsLocationPageState();
}

class _NewsLocationPageState extends State<NewsLocationPage> {
  final n = Get.put(NewsController());
  final m = Get.put(MapController());
  TextEditingController locationController = TextEditingController(text: "");
  Completer<GoogleMapController> _controller = Completer();

  List<Marker> the = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Lokasi Berita").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text("Lokasi Berita").p14b().primary(),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ClipRRect(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(10),
                    ),
                    child: SizedBox(
                        width: Get.width,
                        height: 300,
                        child: GoogleMap(
                          mapType: MapType.normal,
                          myLocationEnabled: true,
                          myLocationButtonEnabled: true,
                          onTap: (LatLng latLng) {
                            the.clear();
                            Marker issueMarker = Marker(
                              markerId: const MarkerId("issueLocation"),
                              position: LatLng(latLng.latitude, latLng.longitude),
                              infoWindow: InfoWindow.noText,
                              icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
                            );
                            the.add(issueMarker);
                            setState(() {
                              n.getAddressFromLatLong(latLng.latitude, latLng.longitude);
                              n.latLong.value = LatLng(latLng.latitude, latLng.longitude);
                            });
                          },
                          initialCameraPosition: m.initialCameraPosition.value,
                          markers: the.map((e) => e).toSet(), // Call a function to create markers
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                        )),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  FlexibleTexField(
                    controller: n.addressController,
                    hintText: "Tuliskan alamat dengan benar",
                    title: "Tulis detail alamat pada form dibawah ini.",
                    isMust: false,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      n.validateLocation(context);
                    },
                    icon: Icons.camera_alt,
                    text: "Upload Foto",
                    iconSize: 20,
                    iconColor: ONetralwhite,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: OPrimaryColor,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      dialogBuilder(
                        context: context,
                        buttonLeft: () {
                          Get.back();
                          n.clearData();
                          Get.off(const HelloConvexAppBar());
                        },
                        buttonRight: () {
                          Get.back();
                        },
                        title: "Batalkan pembuatan pengaduan baru ?",
                        leftTittle: 'Ya',
                        rightTitle: "Tidak",
                        barier: true,
                      );
                    },
                    icon: Icons.clear,
                    text: "Batal",
                    textColor: ORedColor,
                    iconSize: 20,
                    iconColor: ORedColor,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: ONetralwhite,
                    outlineColor: ORedColor,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
