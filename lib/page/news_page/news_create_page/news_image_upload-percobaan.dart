import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsImageUploadPagePercobaan extends StatefulWidget {
  const NewsImageUploadPagePercobaan({super.key});
  @override
  State<NewsImageUploadPagePercobaan> createState() => _NewsImageUploadPageState();
}

class _NewsImageUploadPageState extends State<NewsImageUploadPagePercobaan> {
  List<TextEditingController> _controllers = [];
  List<TextField> _fields = [];
  // List<File> _filePhotos = [];
  List<Widget> _photos = [];
  // File? filePhoto;

  int maxField = 0;

  @override
  void dispose() {
    for (final controller in _controllers) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Upload Foto").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text("Pilih Foto").p14b().primary(),
                    ],
                  ),
                  _listView(),
                  _addTile(),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {},
                    icon: Icons.output,
                    text: "Rangkuman Berita",
                    iconSize: 20,
                    iconColor: ONetralwhite,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: OPrimaryColor,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {},
                    icon: Icons.clear,
                    text: "Batal",
                    textColor: ORedColor,
                    iconSize: 20,
                    iconColor: ORedColor,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: ONetralwhite,
                    outlineColor: ORedColor,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _listView() {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: _fields.length,
      itemBuilder: (context, index) {
        return Column(
          children: [
            Row(
              children: [
                _photos[index],
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    alignment: Alignment.center,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(
                        maxHeight: 80.0,
                        minHeight: 10,
                      ),
                      child: _fields[index],
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      if (maxField > 0) {
                        maxField--;
                        print("Removed index - ${index + 1}");
                        _fields.removeAt(index);
                        _controllers.removeAt(index);
                      }
                    });
                  },
                  child: const Icon(
                    Icons.delete,
                    textDirection: TextDirection.rtl,
                    size: 20,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            )
          ],
        );
      },
    );
  }

  Widget _addTile() {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            File? filePhoto;
            final ImagePicker picker = ImagePicker();
            final photosSite = Column(
              children: [
                GestureDetector(
                  onTap: () {
                    showDialog(
                      barrierDismissible: true,
                      context: context,
                      builder: (c) => AlertDialog(
                        // title: Container(),
                        content: Container(
                          // color: Color.fromRGBO(255, 255, 255, 1),
                          color: ONetralwhite,
                          height: 80,
                          child: Column(
                            // mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              InkWell(
                                onTap: () async {
                                  imageCache.clear();
                                  imageCache.clearLiveImages();
                                  Navigator.pop(context);
                                  // File? file = await Get.to(() => const CameraOverlay('identitas'));
                                  final XFile? image = await picker.pickImage(
                                    source: ImageSource.camera,
                                  );
                                  // print("HAHA");
                                  if (image != null) {
                                    setState(
                                      () {
                                        print("HIHI");
                                        filePhoto = File(image.path);
                                        if (filePhoto != null) {
                                          print("filePhoto NOT NULL");
                                          print(filePhoto);
                                        }
                                        // print(image.path.toString());
                                        // print(filePhoto?.path.toString());

                                        // p.filePhoto = File(image.path);

                                        // filePhoto = image as File?;
                                      },
                                    );
                                  }
                                },
                                child: const Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10.0),
                                  child: Text(
                                    "Kamera",
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () async {
                                  imageCache.clear();
                                  imageCache.clearLiveImages();
                                  Navigator.pop(context);
                                  final XFile? image = await picker.pickImage(source: ImageSource.gallery);
                                  // print("HAHA");
                                  if (image != null) {
                                    setState(
                                      () {
                                        print("HIHI");
                                        filePhoto = File(image.path);
                                        if (filePhoto != null) {
                                          print("filePhoto NOT NULL");
                                          print(filePhoto);
                                          print("===========");
                                        }
                                        // print(image.path.toString());
                                        // print(filePhoto?.path.toString());
                                        // p.filePhoto = File(image.path);
                                        // print(p.filePhoto.toString());
                                      },
                                    );
                                  }
                                },
                                child: const Text("Galeri"),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(10),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: OCalmGrey,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      width: 80,
                      height: 80,
                      // ignore: unnecessary_null_comparison
                      child: (filePhoto == null)
                          ? 
                          Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Icon(Icons.camera_alt),
                                const Text("Tambah Foto").p9r(),
                              ],
                            )
                          : Image.file(
                              fit: BoxFit.fitWidth,
                              File(filePhoto.path),
                            ),
                    ),
                  ),
                ),
              ],
            );
            final controller = TextEditingController();
            final field = TextField(
              enabled: true,
              maxLines: null,
              controller: controller,
              textAlignVertical: TextAlignVertical.center,
              style: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
              decoration: InputDecoration(
                hintStyle: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                // hintText: widget.hintText,
                filled: true,
                fillColor: Colors.transparent,
                contentPadding: const EdgeInsets.only(bottom: 0, left: 15),
                focusedBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(width: 1, color: OPrimaryColor),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: OPrimaryColor), //<-- SEE HERE
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            );
            setState(
              () {
                if (maxField <= 4) {
                  maxField++;
                  print(maxField.toString());
                  _controllers.add(controller);
                  _fields.add(field);
                  // _filePhotos.add(filePhoto!);
                  _photos.add(photosSite);
                }
              },
            );
          },
          child: Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                  color: OPrimaryColor,
                  borderRadius: BorderRadius.circular(1000),
                  border: Border.all(
                    color: OPrimaryColor,
                  )),
              child: const Icon(
                Icons.add,
                color: Colors.white,
              )),
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget _okButton() {
    return ElevatedButton(
      onPressed: () async {
        String text = _controllers.where((element) => element.text != "").fold("", (acc, element) => acc += "${element.text}\n");
        final alert = AlertDialog(
          title: Text("Count: ${_controllers.length}"),
          content: Text(text.trim()),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text("OK"),
            ),
          ],
        );
        await showDialog(
          context: context,
          builder: (BuildContext context) => alert,
        );
        setState(() {});
      },
      child: const Text("Submit"),
    );
  }
}
