import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tagbeta/controllers/map_controller.dart';
import 'package:tagbeta/controllers/news_controller.dart';
import 'package:tagbeta/page/navigationbar/navigationbar.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/custom_fixed_form.dart';
import 'package:tagbeta/widget/base/showdialog_two_button.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsSummaryPage extends StatefulWidget {
  const NewsSummaryPage({super.key});
  @override
  State<NewsSummaryPage> createState() => _NewsSummaryPageState();
}

class _NewsSummaryPageState extends State<NewsSummaryPage> {
  final n = Get.put(NewsController());
  final m = Get.put(MapController());
  TextEditingController locationController = TextEditingController(text: "");
  Completer<GoogleMapController> _controller = Completer();

  List<Marker> the = [];

  @override
  void initState() {
    the.add(Marker(
      markerId: const MarkerId("issueLocation"),
      position: n.latLong.value,
      infoWindow: InfoWindow.noText,
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
    ));
    setState(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Rangkuman").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  CustomFixedForm(
                    title: "Judul",
                    content: n.titleController.text,
                    backgroundColor: ONetralwhite,
                  ),
                  CustomFixedForm(
                    title: "Deskripsi",
                    content: n.newsContentController.text,
                    backgroundColor: ONetralwhite,
                  ),
                  Row(
                    children: [
                      const Text("Lokasi Berita").p12m().black(),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(10),
                      ),
                      child: SizedBox(
                          width: Get.width,
                          height: 150,
                          child: GoogleMap(
                            zoomControlsEnabled: false,
                            zoomGesturesEnabled: false,
                            mapType: MapType.normal,
                            myLocationEnabled: false,
                            myLocationButtonEnabled: false,
                            initialCameraPosition: CameraPosition(
                              target: n.latLong.value, // Ganti dengan koordinat yang Anda inginkan
                              zoom: 17.0, // Ganti dengan level zoom yang Anda inginkan
                            ),
                            markers: the.map((e) => e).toSet(), // Call a function to create markers
                            onMapCreated: (GoogleMapController controller) {
                              _controller.complete(controller);
                            },
                          )),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomFixedForm(
                    title: "Alamat",
                    content: n.addressController.text,
                    backgroundColor: ONetralwhite,
                  ),
                  Row(
                    children: [
                      const Text("Foto").p12m().black(),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: n.photos.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Column(
                                children: [
                                  ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: OCalmGrey,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      width: 80,
                                      height: 80,
                                      child: Image.file(
                                        fit: BoxFit.cover,
                                        File(n.photos[index].path),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(
                                      maxHeight: 80.0,
                                      minHeight: 10,
                                    ),
                                    child: TextField(
                                      // enabled: true,
                                      readOnly: true,
                                      maxLines: null,
                                      controller: n.controllers[index],
                                      textAlignVertical: TextAlignVertical.center,
                                      style: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                        hintStyle: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                                        // hintText: widget.hintText,
                                        filled: true,
                                        fillColor: Colors.transparent,
                                        contentPadding: const EdgeInsets.only(bottom: 0, left: 15),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                                          borderSide: BorderSide(width: 1, color: OPrimaryColor),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(width: 1, color: OPrimaryColor), //<-- SEE HERE
                                          borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          )
                        ],
                      );
                    },
                  ),
                  BaseButton(
                    ontap: () {
                      n.validateLocation(context);
                    },
                    icon: Icons.save,
                    text: "Simpan Draft",
                    iconSize: 20,
                    iconColor: ONetralwhite,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: OPrimaryColor,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {},
                    icon: Icons.send,
                    text: "Kirim",
                    textColor: OSecondaryColor,
                    iconSize: 20,
                    iconColor: OSecondaryColor,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: ONetralwhite,
                    outlineColor: OSecondaryColor,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      dialogBuilder(
                        context: context,
                        buttonLeft: () {
                          Get.back();
                          n.clearData();
                          Get.off(const HelloConvexAppBar());
                        },
                        buttonRight: () {
                          Get.back();
                        },
                        title: "Batalkan pembuatan pengaduan baru ?",
                        leftTittle: 'Ya',
                        rightTitle: "Tidak",
                        barier: true,
                      );
                    },
                    icon: Icons.clear,
                    text: "Batal",
                    textColor: ORedColor,
                    iconSize: 20,
                    iconColor: ORedColor,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: ONetralwhite,
                    outlineColor: ORedColor,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
