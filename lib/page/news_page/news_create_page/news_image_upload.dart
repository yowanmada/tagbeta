import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tagbeta/controllers/news_controller.dart';
import 'package:tagbeta/page/navigationbar/navigationbar.dart';
import 'package:tagbeta/page/news_page/news_create_page/news_image_picker_page.dart';
import 'package:tagbeta/page/news_page/news_create_page/news_summary_page.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/showdialog_two_button.dart';
import 'package:tagbeta/widget/base/showdialog_waiting.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsImageUploadPage extends StatefulWidget {
  const NewsImageUploadPage({super.key});
  @override
  State<NewsImageUploadPage> createState() => _NewsImageUploadPageState();
}

class _NewsImageUploadPageState extends State<NewsImageUploadPage> {
  final n = Get.put(NewsController());
  var how;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Upload Foto").p14b().white(),
        leading: const BackButton(
          color: Colors.white,
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text("Pilih Foto").p14b().primary(),
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: n.photos.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Column(
                                children: [
                                  ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: OCalmGrey,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      width: 80,
                                      height: 80,
                                      child: Image.file(
                                        fit: BoxFit.cover,
                                        File(n.photos[index].path),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(
                                      maxHeight: 80.0,
                                      minHeight: 10,
                                    ),
                                    child: TextField(
                                      enabled: true,
                                      maxLines: null,
                                      controller: n.controllers[index],
                                      textAlignVertical: TextAlignVertical.center,
                                      style: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                        hintStyle: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                                        // hintText: widget.hintText,
                                        filled: true,
                                        fillColor: Colors.transparent,
                                        contentPadding: const EdgeInsets.only(bottom: 0, left: 15),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                                          borderSide: BorderSide(width: 1, color: OPrimaryColor),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(width: 1, color: OPrimaryColor), //<-- SEE HERE
                                          borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              BaseButton(
                                ontap: () {
                                  setState(() {
                                    n.photos.removeAt(index);
                                    n.controllers.removeAt(index);
                                  });
                                },
                                icon: Icons.delete,
                                color: ORedColor,
                                iconColor: ONetralwhite,
                                outlineRadius: 4,
                                iconSize: 15,
                                width: 25,
                                height: 25,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          )
                        ],
                      );
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () async {
                      how = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const NewsImagePicker()),
                      );
                      setState(() {});
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: OCalmGrey,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      width: 80,
                      height: 80,
                      // ignore: unnecessary_null_comparison
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(Icons.camera_alt),
                          const Text("Tambah Foto").p9r(),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
                      Future.delayed(
                        const Duration(seconds: 1),
                        () {
                          Get.back();
                          Get.to(const NewsSummaryPage());
                        },
                      );
                    },
                    icon: Icons.output,
                    text: "Rangkuman Berita",
                    iconSize: 20,
                    iconColor: ONetralwhite,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: OPrimaryColor,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      dialogBuilder(
                        context: context,
                        buttonLeft: () {
                          Get.back();
                          n.clearData();
                          Get.off(const HelloConvexAppBar());
                        },
                        buttonRight: () {
                          Get.back();
                        },
                        title: "Batalkan pembuatan pengaduan baru ?",
                        leftTittle: 'Ya',
                        rightTitle: "Tidak",
                        barier: true,
                      );
                    },
                    icon: Icons.clear,
                    text: "Batal",
                    textColor: ORedColor,
                    iconSize: 20,
                    iconColor: ORedColor,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: ONetralwhite,
                    outlineColor: ORedColor,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
