import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tagbeta/controllers/news_controller.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/showdialog_waiting.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsImagePicker extends StatefulWidget {
  const NewsImagePicker({super.key});

  @override
  State<NewsImagePicker> createState() => _NewsImagePickerState();
}

class _NewsImagePickerState extends State<NewsImagePicker> {
  final n = Get.put(NewsController());
  File? filePhoto;
  final ImagePicker _picker = ImagePicker();
  final controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Foto").p14b().white(),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context, true);
          },
          child: const BackButton(
            color: Colors.white, // <-- SEE HERE
          ),
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  child: SizedBox(
                    width: Get.width,
                    height: 500,
                    child: (filePhoto == null)
                        ? Container(
                            color: Colors.grey,
                            child: const Icon(
                              Icons.camera_alt,
                              size: 100,
                              color: Colors.white,
                            ),
                          )
                        : Image.file(
                            File(filePhoto!.path),
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                  maxHeight: 80.0,
                  minHeight: 10,
                ),
                child: TextField(
                  enabled: true,
                  maxLines: null,
                  controller: controller,
                  textAlignVertical: TextAlignVertical.center,
                  style: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                  decoration: InputDecoration(
                    hintStyle: GoogleFonts.poppins(color: const Color(0xff868686), fontSize: 12, fontWeight: FontWeight.w600),
                    hintText: "Keterangan ....",
                    filled: true,
                    fillColor: Colors.transparent,
                    contentPadding: const EdgeInsets.only(bottom: 0, left: 15),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(width: 1, color: OConcenGrey),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: OConcenGrey), //<-- SEE HERE
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: BaseButton(
                      height: 35,
                      icon: Icons.camera_alt_rounded,
                      iconColor: ONetralwhite,
                      text: "Foto",
                      ontap: () async {
                        showDialog(
                          barrierDismissible: true,
                          context: context,
                          builder: (c) => AlertDialog(
                            content: Container(
                              color: ONetralwhite,
                              height: 80,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      Navigator.pop(context);
                                      final XFile? image = await _picker.pickImage(
                                        source: ImageSource.camera,
                                      );
                                      if (image != null) {
                                        setState(() {
                                          filePhoto = File(image.path);
                                        });
                                      }
                                    },
                                    child: const Padding(
                                      padding: EdgeInsets.symmetric(vertical: 10.0),
                                      child: Text(
                                        "Kamera",
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      Navigator.pop(context);
                                      final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
                                      if (image != null) {
                                        setState(() {
                                          filePhoto = File(image.path);
                                        });
                                      }
                                    },
                                    child: const Text("Galeri"),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 1,
                    child: BaseButton(
                      height: 35,
                      ontap: () {
                        if (filePhoto == null) {
                          waitingDialog(title: 'Silahkan Pilih Foto Dahulu', barier: true, context: context, icon: Icons.warning);
                          Future.delayed(
                            const Duration(seconds: 2),
                            () async {
                              Navigator.pop(
                                context,
                              );
                            },
                          );
                        } else {
                          waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
                          Future.delayed(
                            const Duration(seconds: 2),
                            () async {
                              setState(
                                () {
                                  n.controllers.add(controller);
                                  n.photos.add(filePhoto!);
                                  Navigator.pop(
                                    context,
                                  );
                                  Navigator.pop(context, true);
                                },
                              );
                            },
                          );
                        }
                      },
                      text: "Simpan",
                      icon: Icons.save,
                      iconColor: ONetralwhite,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
