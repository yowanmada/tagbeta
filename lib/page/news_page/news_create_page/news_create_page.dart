import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/controllers/news_controller.dart';
import 'package:tagbeta/page/navigationbar/navigationbar.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/base/custom_dropdown.dart';
import 'package:tagbeta/widget/base/custom_flexible_form.dart';
import 'package:tagbeta/widget/base/custom_form.dart';
import 'package:tagbeta/widget/base/showdialog_two_button.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsCreatePage extends StatefulWidget {
  const NewsCreatePage({super.key});

  @override
  State<NewsCreatePage> createState() => _NewsCreatePageState();
}

class _NewsCreatePageState extends State<NewsCreatePage> {
  final n = Get.put(NewsController());
  var newsCategory = ["Ekraf", "Event", "Hubungan Masyarakat", "Infrastruktur", "Kebersihan", "Kesehatan", "Kriminal", "Kuliner dan Jajanan", "Lingkungan Hidup", "Periwisata", "Perikanan Kelautan", "Perlindungan Ibu anak", "Pertanian", "Peternakan"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Tulis Berita").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  CustomForm(
                    controller: n.titleController,
                    // hintText: "",
                    hintText: "Tuliskan Judul disini",
                    title: "Judul",
                    isMust: true,
                  ),
                  CustomDropDown(
                    title: "Kategori Berita",
                    items: newsCategory,
                    firstItem: newsCategory[0],
                    controller: n.newsCategoryController,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FlexibleTexField(
                    controller: n.newsContentController,
                    hintText: "Isikan berita disini",
                    title: "Isi Berita",
                    isMust: true,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      n.validateData(context);
                    },
                    icon: Icons.location_on,
                    text: "Tentukan Lokasi",
                    iconSize: 20,
                    iconColor: ONetralwhite,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: OPrimaryColor,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BaseButton(
                    ontap: () {
                      dialogBuilder(
                        context: context,
                        buttonLeft: () {
                          Get.back();
                          n.clearData();
                          Get.off(const HelloConvexAppBar());
                        },
                        buttonRight: () {
                          Get.back();
                        },
                        title: "Batalkan pembuatan pengaduan baru ?",
                        leftTittle: 'Ya',
                        rightTitle: "Tidak",
                        barier: true,
                      );
                    },
                    icon: Icons.clear,
                    text: "Batal",
                    textColor: ORedColor,
                    iconSize: 20,
                    iconColor: ORedColor,
                    height: 35,
                    textSize: 10,
                    outlineRadius: 1000,
                    textWeight: FontWeight.w700,
                    color: ONetralwhite,
                    outlineColor: ORedColor,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
