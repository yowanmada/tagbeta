import 'package:flutter/material.dart';
import 'package:tagbeta/page/news_page/body/news_view.dart';
import 'package:tagbeta/page/news_page/body/public_view.dart';
import 'package:tagbeta/utils/colors.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  // fungsi untuk inisialisasi tabController / swipeable topnavigation bar,
  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    //Function for HIT List News with some parameter from Filter & squence.
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // String image = "assets/images/gambar1.jpg";
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 20,
            color: OPrimaryColor,
          ),
          Column(
            children: [
              Container(
                height: 20,
                color: OPrimaryColor,
              ),
              Container(
                color: OPrimaryColor,
                height: 40,
                child: TabBar(
                  controller: _tabController,
                  indicator: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 3,
                        color: OTextSecondColor,
                      ),
                    ),
                    color: OPrimaryColor,
                  ),
                  labelColor: ONetralwhite,
                  indicatorColor: OPrimaryColor,
                  dividerColor: OPrimaryColor,
                  unselectedLabelColor: ONetralwhite,
                  tabs: const [
                    // first tab [you can add an icon using the icon property]
                    Tab(
                      text: 'Publik',
                    ),

                    // second tab [you can add an icon using the icon property]
                    Tab(
                      text: 'Beritaku',
                    ),
                  ],
                ),
              ),
            ],
          ),
          // tab bar view here
          Expanded(
            flex: 1,
            child: TabBarView(
              controller: _tabController,
              children: const [
                PublikView(),
                NewsView(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
