import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsDetailPage extends StatefulWidget {
  const NewsDetailPage({super.key});

  @override
  State<NewsDetailPage> createState() => _NewsDetailPageState();
}

class _NewsDetailPageState extends State<NewsDetailPage> {
  @override
  Widget build(BuildContext context) {
    final image = "assets/images/gambar1.jpg".obs;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OPrimaryColor,
        title: const Text("Detail Berita").p14b().white(),
        leading: const BackButton(
          color: Colors.white, // <-- SEE HERE
        ),
        titleSpacing: 4,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              width: Get.width,
              height: 300,
              child: FittedBox(
                fit: BoxFit.cover,
                child: Obx(
                  () => Image.asset(
                    image.value,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: Get.width,
              height: 72,
              child: ListView.builder(
                itemCount: 4,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) => Row(
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            image.value = "assets/images/gambar${index + 1}.jpg";
                          },
                          child: SizedBox(
                            width: 100,
                            height: 72,
                            child: Image.asset(
                              "assets/images/gambar${index + 1}.jpg",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text("Banjir Bandang").p16b().primary(),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "mengesankan. Dalam perencanaannya, perhatian utama diberikan kepada pengguna mobil, terutama mereka yang memiliki Pajero Sport versi terbaru, yaitu TDR Magnum. Jalan ini dirancang dengan berbagai fitur khusus, seperti jalan yang lebih lebar, aspal berkualitas tinggi, dan tikungan yang disesuaikan untuk memberikan pengalaman berkendara yang optimal. Selain itu, fasilitas tambahan seperti pompa bensin modern dan stasiun pengisian daya untuk mobil listrik telah ditempatkan secara strategis di sepanjang Kandangan Raya Bulak Banteng, memudahkan pengguna mobil Pajero Sport TDR Magnum untuk mengisi bahan bakar atau mengisi daya dengan cepat dan efisien.Dengan demikian, infrastruktur ini tidak hanya meningkatkan mobilitas pengguna mobil Pajero Sport TDR Magnum, tetapi juga memberikan dampak positif pada ekonomi dan kualitas hidup di sekitar wilayah tersebut. Selain itu, infrastruktur yang modern ini juga mencerminkan komitmen terhadap penggunaan teknologi terbaru dan perhatian pada keamanan dan kenyamanan pengguna jalan.",
                    textAlign: TextAlign.justify,
                  ).p12r().primary(),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
