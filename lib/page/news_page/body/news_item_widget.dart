import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tagbeta/controllers/global_controller.dart';
import 'package:tagbeta/page/news_page/news_detail_page/news_detail_page.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsItem extends StatelessWidget {
  const NewsItem({
    super.key,
    required this.myNews,
  });

  final bool myNews;

  @override
  Widget build(BuildContext context) {
    final c = Get.put(GlobalController());

    return Expanded(
      flex: 1,
      child: ListView.builder(
        itemCount: 3,
        padding: EdgeInsets.zero,
        itemBuilder: (context, index) => Column(
          children: [
            Container(
              width: Get.width - 32,
              decoration: BoxDecoration(
                color: ONetralwhite,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: OCalmGrey,
                    offset: const Offset(
                      10.0,
                      10.0,
                    ),
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 8,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "(Bencana Alam) - Banjir Bandang Aceh",
                        ).p14b().primary(),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    if (myNews) ...[
                      Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 1,
                              horizontal: 4,
                            ),
                            decoration: BoxDecoration(
                              color: const Color(0xff80DEFF),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: const Text("Draft"),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.timelapse,
                            size: 15,
                            color: OConcenGrey,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          const Text("20 Jam yang lalu").p10r().concernGrey(),
                        ],
                      ),
                    ] else ...[
                      Row(
                        children: [
                          Icon(
                            Icons.timelapse,
                            size: 15,
                            color: OConcenGrey,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          const Text("26 Januari 2023 - 09:32").p10r().concernGrey(),
                        ],
                      ),
                    ],
                    const SizedBox(
                      height: 5,
                    ),
                    ClipRRect(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(10),
                      ),
                      child: SizedBox(
                        width: Get.width,
                        height: 137,
                        child: Obx(
                          () => FittedBox(
                            fit: BoxFit.cover,
                            child: Image.asset(
                              c.image.value,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      width: Get.width,
                      height: 72,
                      child: ListView.builder(
                        itemCount: 4,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) => Row(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    c.image.value = "assets/images/gambar${index + 1}.jpg";
                                  },
                                  child: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    child: SizedBox(
                                      width: 100,
                                      height: 72,
                                      child: FittedBox(
                                        fit: BoxFit.cover,
                                        child: Image.asset(
                                          "assets/images/gambar${index + 1}.jpg",
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        const Text("Keterangan").p12b().primary(),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: RichText(
                            text: TextSpan(
                              text: 'Kebanjiran -',
                              style: GoogleFonts.poppins(fontSize: 12, fontWeight: FontWeight.w600, color: OPrimaryColor),
                              children: <TextSpan>[
                                TextSpan(
                                  text: ' Warga Kupang Lima Simo Indah Jaya Barat mendapat kecaman karena maraknya pembuangan sampah sembarangan!',
                                  style: GoogleFonts.poppins(fontSize: 12, fontWeight: FontWeight.w400, color: OPrimaryColor),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Get.to(const NewsDetailPage());
                          },
                          child: const Text(
                            "Baca Selengkapnya",
                            style: TextStyle(height: 1.5),
                            maxLines: 3,
                          ).p10r().secondary(),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Divider(
                      color: OCalmGrey,
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        const Text("Lokasi").p12b().primary(),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.location_on,
                          color: OTextSecondColor,
                          size: 15,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          flex: 1,
                          child: const Text(
                            overflow: TextOverflow.ellipsis,
                            "Jl. Jaya Wijaya,kusuma, Klender Jara Wimtu, Kabupaten Solaria interfensi solusi wakwkakak",
                            style: TextStyle(height: 1.5),
                            maxLines: 2,
                          ).p12r().primary(),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: SizedBox(
                            width: 49,
                            height: 44,
                            child: Image.asset(
                              "assets/images/map.png",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Divider(
                      color: OCalmGrey,
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.thumb_up,
                          color: OPrimaryColor,
                        ),
                        Container(
                          height: 15,
                          decoration: BoxDecoration(
                            border: Border(
                              left: BorderSide(
                                color: OCalmGrey,
                              ),
                            ),
                          ),
                        ),
                        Icon(
                          Icons.comment,
                          color: OPrimaryColor,
                        )
                      ],
                    ),
                    if (myNews) ...[
                      Divider(
                        color: OCalmGrey,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: BaseButton(
                              ontap: () {},
                              text: "Ubah",
                              height: 26,
                              textSize: 10,
                              outlineRadius: 20,
                              textWeight: FontWeight.w700,
                              color: OPrimaryColor,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            flex: 1,
                            child: BaseButton(
                              ontap: () {},
                              text: "Kirim",
                              height: 26,
                              textSize: 10,
                              outlineRadius: 20,
                              textWeight: FontWeight.w700,
                              color: OSecondaryColor,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          BaseButton(
                            ontap: () {},
                            icon: Icons.delete_forever_sharp,
                            iconSize: 20,
                            iconColor: ONetralwhite,
                            height: 26,
                            width: 26,
                            textSize: 10,
                            outlineRadius: 1000,
                            textWeight: FontWeight.w700,
                            color: const Color(0xffFF4342),
                          ),
                        ],
                      )
                    ]
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
