import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/page/news_page/body/news_item_widget.dart';
import 'package:tagbeta/page/news_page/news_create_page/news_create_page.dart';
import 'package:tagbeta/page/news_page/news_filter_page/news_filter_page.dart';
import 'package:tagbeta/utils/colors.dart';
import 'package:tagbeta/widget/base/button_base.dart';
import 'package:tagbeta/widget/extention/ext_text.dart';

class NewsView extends StatefulWidget {
  const NewsView({super.key});

  @override
  State<NewsView> createState() => _NewsViewState();
}

class _NewsViewState extends State<NewsView> {
  List<String> reportType = ["COVID 19", "Bencana Alam", "Darurat Medis", "Kebakaran", "Kecelakaan", "KDRT", "Pembegalan", "Jambak-Jambak an", "Metamorfosis", "Aksi Penghancuran", "Fasilitas Publik Rusak", "COVID 19", "Bencana Alam", "Darurat Medis", "Pembegalan", "Jambak-Jambak an"];
  var result;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Container(
              width: Get.width,
              height: 60,
              decoration: BoxDecoration(
                color: ONetralwhite,
                boxShadow: [
                  BoxShadow(
                    color: OCalmGrey,
                    offset: const Offset(
                      10.0,
                      10.0,
                    ),
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    BaseButton(
                      ontap: () async {
                        result = await Navigator.push(
                          context,
                          // Create the SelectionScreen in the next step.
                          MaterialPageRoute(builder: (context) => const NewsFilterPage()),
                        );
                        setState(() {});
                        // print(result);
                      },
                      icon: Icons.filter_alt,
                      iconColor: ONetralwhite,
                      width: 35,
                      height: 35,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    BaseButton(
                      ontap: () {},
                      text: "Urutan : Terbaru",
                      textSize: 14,
                      textWeight: FontWeight.w400,
                      width: 136,
                      height: 35,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    if (result == null || result.isEmpty) ...[
                      Container(),
                    ] else ...[
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          height: 30,
                          child: ListView.builder(
                            itemCount: result.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) => Row(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                      decoration: BoxDecoration(
                                        color: ONetralwhite,
                                        border: Border.all(color: ORedColor),
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(5),
                                        ),
                                      ),
                                      child: Row(
                                        children: [
                                          Text(reportType[result[index]]).p14m(),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                result.removeAt(index);
                                              });
                                            },
                                            child: Icon(
                                              Icons.clear,
                                              color: ORedColor,
                                              size: 15,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ]
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            const NewsItem(
              myNews: true,
            )
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BaseButton(
                  ontap: () {
                    Get.to(const NewsCreatePage());
                  },
                  text: "Tulis Berita",
                  icon: Icons.create,
                  iconColor: ONetralwhite,
                  textSize: 14,
                  color: OTextSecondColor,
                  outlineRadius: 20,
                  textWeight: FontWeight.w400,
                  width: 140,
                  height: 40,
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            )
          ],
        ),
      ],
    );
  }
}
