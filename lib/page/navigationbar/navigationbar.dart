import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/controllers/global_controller.dart';
import 'package:tagbeta/utils/colors.dart';

// Halaman pertama pada aplikasi yakni convex app bar,
class HelloConvexAppBar extends StatefulWidget {
  const HelloConvexAppBar({super.key});

  @override
  State<HelloConvexAppBar> createState() => _HelloConvexAppBarState();
}

class _HelloConvexAppBarState extends State<HelloConvexAppBar> {
  final c = Get.put(GlobalController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() => c.children[c.tabHomeIndex.toInt()]),
      bottomNavigationBar: StyleProvider(
        style: Style(),
        child: Obx(() => ConvexAppBar(
            style: TabStyle.reactCircle,
            curveSize: 80,
            height: 62,
            // cornerRadius: 20,
            items: [
              TabItem(
                icon: Image.asset("assets/images/Runner-1.png"),
                activeIcon: Image.asset("assets/images/Runner.png"),
                title: "Aktivitas",
              ),
              TabItem(
                icon: Image.asset("assets/images/Address.png"),
                activeIcon: Image.asset("assets/images/Address-1.png"),
                title: "Peta",
              ),
              TabItem(
                icon: Image.asset("assets/images/News.png"),
                activeIcon: Image.asset("assets/images/News-1.png"),
                title: "Berita",
              ),
              TabItem(
                icon: Image.asset("assets/images/Metrics.png"),
                activeIcon: Image.asset("assets/images/Metrics-1.png"),
                title: "Statistik",
              ),
              TabItem(
                icon: Image.asset("assets/images/Customer.png"),
                activeIcon: Image.asset("assets/images/Customer-1.png"),
                title: "Profil",
              ),
            ],
            activeColor: ONetralwhite,
            color: const Color.fromARGB(255, 212, 211, 211),
            backgroundColor: OPrimaryColor,
            initialActiveIndex: c.tabHomeIndex.value,
            disableDefaultTabController: true,
            onTap: (int i) async {
              c.onTabTapped(i);

              // Dicek apakah user telah login atau belum. Jika sudah maka user dapat mengakses halaman yang tidak dapat diakses ketika user belum login.
              // String? token = await c.getToken();
              // if (i == 1 || i == 2 || i == 4) {
              //   if (token == null) {
              //     c.onTabTapped(0);
              //     Get.offAll(const HelloConvexAppBar());
              //     // Get.to(const LoginPage());
              //   } else {
              //     c.onTabTapped(i);
              //   }
              // } else {
              //   c.onTabTapped(i);
              // }
            })),
      ),
    );
  }
}

class Style extends StyleHook {
  @override
  double get activeIconSize => 5;
  @override
  double get activeIconMargin => 5;
  @override
  double get iconSize => 30;
  @override
  TextStyle textStyle(Color color, String? fontFamily) {
    return TextStyle(fontSize: 12, color: color);
  }
}
