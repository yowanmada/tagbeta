import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tagbeta/page/news_page/news_create_page/news_image_upload.dart';
import 'package:tagbeta/page/news_page/news_create_page/news_location_page.dart';
import 'package:tagbeta/widget/base/showdialog_waiting.dart';

class NewsController extends GetxController {
  TextEditingController titleController = TextEditingController(text: "");
  TextEditingController newsCategoryController = TextEditingController(text: "");
  TextEditingController newsContentController = TextEditingController(text: "");
  TextEditingController addressController = TextEditingController(text: "");
  List<TextEditingController> controllers = [];
  List<File> photos = [];
  final latLong = LatLng(0, 0).obs;

  clearData() {
    titleController.clear();
    newsCategoryController.clear();
    newsContentController.clear();
    addressController.clear();
    latLong.value = LatLng(0, 0);
    controllers.clear();
    photos.clear();
  }

  validateData(BuildContext context) {
    if (titleController.text.isNotEmpty && newsContentController.text.isNotEmpty && newsCategoryController.text.isNotEmpty) {
      waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
      Future.delayed(
        const Duration(seconds: 1),
        () {
          Get.back();
          Get.to(const NewsLocationPage());
        },
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Pastikan tidak ada kolom yang kosong sebelum melanjutkan',
          ),
          backgroundColor: Colors.black87,
        ),
      );
    }
  }

  validateLocation(BuildContext context) {
    if (addressController.text.isNotEmpty && latLong != const LatLng(0, 0)) {
      waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
      Future.delayed(
        const Duration(seconds: 1),
        () {
          Get.back();
          Get.to(const NewsImageUploadPage());
        },
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Mohon Pilih Lokasi Pada Maps & Pastikan Alamat Anda Sudah Tepat',
          ),
          backgroundColor: Colors.black87,
        ),
      );
    }
  }

  saveDraft(BuildContext context) {
    if (titleController.text.isNotEmpty && newsContentController.text.isNotEmpty && newsCategoryController.text.isNotEmpty && addressController.text.isNotEmpty && latLong != const LatLng(0, 0)) {
      waitingDialog(title: 'Mohon Tunggu Sebentar', barier: true, context: context);
      Future.delayed(
        const Duration(seconds: 2),
        () {
          Get.back();
          Get.to(const NewsImageUploadPage());
        },
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Mohon Pilih Lokasi Pada Maps & Pastikan Alamat Anda Sudah Tepat',
          ),
          backgroundColor: Colors.black87,
        ),
      );
    }
  }

  Future<void> getAddressFromLatLong(double latitude, double longitude) async {
    //TODO : JIKA bukan web, get addres from plugin
    List<Placemark> placemarks = await placemarkFromCoordinates(latitude, longitude);
    print(placemarks);
    Placemark place = placemarks[0];
    var address = '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    addressController.text = address;
  }
}
