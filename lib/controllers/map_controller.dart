import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tagbeta/controllers/global_controller.dart';

class MapController extends GetxController {
  var c = Get.put(GlobalController());
  // final Dio dio = Dio();

  final centerCamera = LatLng(-7.2909443, 112.6306296).obs;

  final initialCameraPosition = CameraPosition(
    target: LatLng(-0.789275, 113.921327), // Ganti dengan koordinat yang Anda inginkan
    zoom: 5.0, // Ganti dengan level zoom yang Anda inginkan
  ).obs;

  // final emailController = TextEditingController(text: '');
  // final nomorController = TextEditingController(text: '');
  // final firstNameController = TextEditingController(text: '');
  // final lastNameController = TextEditingController(text: '');
  // final genderController = TextEditingController(text: '');
  // final tanggalController = TextEditingController(text: '');
  // final passwordController = TextEditingController(text: "");
}
