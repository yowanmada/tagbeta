import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tagbeta/page/navigationbar/navigationbar.dart';
// import 'package:tagbeta/page/splash_page/splash_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HelloConvexAppBar(),
    );
  }
}
